## Propuesta cambio de enfoque de estilos cells

En la actualidad se generan componentes de forma individual, los componentes tienen una serie de estilos propios por defecto y se utilizan variables  y mixins para permitir que otros usuarios puedan configurar el componente.

ejemplo de uso de variable en nuestro componente  
```javascript
  background: var(--cells-KMW_colour, #FFFFFF);
```

ejemplo de uso de mixin en nuestro componente
```javascript
.panel{
  display: flex;
  flex-direction: column;
  max-width: 90%;
  margin: 0 auto 1.5rem auto;
  @apply(--cells-card-detail-panel);
}
```

Siguiendo las convenciones de polymer, se indican en los mixin el nombre del componente  para que no entre en conflicto con otros componentes.

En la documentaci�n de polymer indican que lso componentes no deberian afectar al resto de la pagina y que si alguien pone un estilo en la pagina, estos tampoco deberian afectar a nuestro componente.

Aun siendo esta premisa correcta en su  idea original, en una aplicacion no utilizamos los componentes solamente de forma aislada, tambien los utilizamos en conjuncion con otros y deberia de haber una homogeneidad en la forma de presentaci�n de todos los componentes 

En la documentaci�n de polymer tambien indica que se pueden crear themes para facilittar el desarrollo, de hecho actualmente hay varios, theme-base, el de glomo, el de ngob...

Todos estos temas tienden hacia un modelo de desarrollo que se ha utilizado en otras aplicaciones bbva, que es tener una hoja de estilos muy grande, con todo lo que podemos necesitar y a la que vamos agregando reglas segun necesitamos usar componentes.

Otros framworks de css basados en la productividad utilizan tecnicas para facilitar el desarrollo, por ejemplo en bootstrap tienes clases predeterminadas para dar el aspecto general a tus componentes y clases "helpers" para no tener que repetir una y otra vez el mismo pading de 5px.

#### Ejemplos de comfiguraciones globales para frameworks

 - foundation http://foundation.zurb.com/sites/docs/global.html
 - bootstrap http://getbootstrap.com/customize/
 - materialize http://materializecss.com/sass.html


Adicionalmente a lo comentado anteriormente trabajamos con multiples equipos de UX, que aunque intentan sincronizarse, actualmente se ven diferencias claras en los entregables que llegan a los diferentes equipos.

Todo esto ha llevado a una situacion donde tenemos por ejemplo en el theme de glomo 52 colores y a que los css de los diferentes componentes son una amalgama de reglas de css, concretamente en mi proyecto hemos hecho un css de 260 lineas.

#### Lista de cambios inicialmente propuestos
reducir el numero de colores permitidos a 

 - primario
 - secundarios
 - acento
 - exito
 - aviso
 - peligro 
 - negro 
 - blanco
 - gris 

generar reglas predecibles para los elementos 

 - primary-title
 - secondary-title
 - primary-text
 - secondary-text
 - primary-link 

y posiblemente sus variedades de 

 - exito
 - aviso
 - peligro

generar una serie de clases helpers para facilitar la disposicion de los elementos facilitando el uso de flex en los componentes  

 - container-horizontal
 - container-vertical
 - container-justify

generar una serie de clases helpers para indicar el numero de columnas que debe de ocupar un componente de ancho (hasta 12)

tener definidas clases especificas para los diferentes tama�os responsive  

 - movil 
 - tablet
 - escritorio
 - pantalla alta  resolucion 

de forma que cuando estas calses esten en el body de nuestra aplicacion podamos cambiar: 

 - el numero de columnas que ocupa un contenedor
 - si esta visible para esa resolucion 
 - si se le aplica desplazamiento push o pull para esa resolucion 

generar clases helpers para los tama�os de letra y con los desplazamientos (pading y margin) mas habituales  

 - size-small
 - size-medium
 - size-big


#### ejemplo de definicion en nuesto theme 
```html
<!-- shared-styles.html -->
<dom-module id="shared-styles">
  <template>
    <style>
        :host {
          --size-small: 0.8rem;
          --size-normal: 1rem;
          --size-big:1.2rem;
          --primary-color: #2a86ca;

          --primary-title:{
            font-size:var(--size-big, 1.5em);
            color:var(--primary-color, #0981c0);
            margin-top:var(--size-normal, 1rem);
          };
      }
    </style>
  </template>
</dom-module>
```


#### ejemplo de uso de las clases en nuestro componente 
```html
<style include="shared-styles"></style>
  <style is="custom-style">
    .primary-title{
      text-align:center;
      font-size:var(--size-big, 1.5rem);
      color:var(--primary-color, #0981c0);
      margin-top:var(--size-normal, 1.5rem);
      @apply(--primary-title);
  
    }
     .....
  </style>
  <template>
```

ejemplo de html usando clases 
```html
  <div class="card-content container-vertical product-card">   
    <paper-item class="primary-title">Cuenta n�mina � 0987</paper-item>
      <paper-item class="primary-text">Disponible</paper-item>
      <paper-item class="secondary-title">$45,532</paper-item>
          <paper-icon-item class="primary-link">
        <iron-icon icon="star" item-icon></iron-icon>
            <span class="red">Ir a Zona N�mina</span>
      </paper-icon-item>
      </div>
    </paper-card>
  </template>
```
en caso de que otro proyecto cells defina un estilo diferente al de coronita, solo es necesario que cree su theme y sobrescriba las clases genericas creando su propio theme 
```css
/* estas reglas las personalizo para mi app*/
--size-big:1.2rem;
--primary-color: #F4F7F5;

/* estas reglas las he dejado igual*/
--primary-title{
	font-size:var(--size-big, 1.5rem);
	color:var(--primary-color, #F4F7F5);
	margin-top:var(--size-normal, 1rem);
}
```

#### prueba de concepto 
http://codepen.io/guzmanator/pen/xgWdPv?editors=1100


#### ejemplo de como se ponen las clases genericas del theme en bootstrap
```html
<div class="dropdown">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    Dropdown
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="#">Action</a></li>
    <li><a href="#">Another action</a></li>
    <li><a href="#">Something else here</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="#">Separated link</a></li>
  </ul>
</div>
```

#### ejemplo de como se ponen las clases genericas del theme en foundation
```html
<div class="card">
  <div class="card-divider">
    This is a header
  </div>
  <img src="assets/img/generic/rectangle-1.jpg">
  <div class="card-section">
    <h4>This is a card.</h4>
    <p>It has an easy to override visual style, and is appropriately subdued.</p>
  </div>
</div>
```

#### ejemplo de como se ponen las clases genericas del theme en  materialize 
```html
<div class="row">
  <div class="col s12 m6">
    <div class="card blue-grey darken-1">
      <div class="card-content white-text">
        <span class="card-title">Card Title</span>
        <p>I am a very simple card. I am good at containing small bits of information.
        I am convenient because I require little markup to use effectively.</p>
      </div>
      <div class="card-action">
        <a href="#">This is a link</a>
        <a href="#">This is a link</a>
      </div>
    </div>
  </div>
</div>
```

#### ejemplo de como bootstrap define las rejillas 
``` css
$grid-columns:      12;
$grid-gutter-width-base: 30px;

$grid-gutter-widths: (
  xs: $grid-gutter-width-base, // 30px
  sm: $grid-gutter-width-base, // 30px
  md: $grid-gutter-width-base, // 30px
  lg: $grid-gutter-width-base, // 30px
  xl: $grid-gutter-width-base  // 30px
)

$grid-breakpoints: (
  // Extra small screen / phone
  xs: 0,
  // Small screen / phone
  sm: 576px,
  // Medium screen / tablet
  md: 768px,
  // Large screen / desktop
  lg: 992px,
  // Extra large screen / wide desktop
  xl: 1200px
);

$container-max-widths: (
  sm: 540px,
  md: 720px,
  lg: 960px,
  xl: 1140px
);
```

#### ejemplo de como define foundation los colores de sus themes 
```css
$foundation-palette: (
  primary: #1779ba,
  secondary: #767676,
  success: #3adb76,
  warning: #ffae00,
  alert: #cc4b37,
);
```

#### ejemplo de helpers de materialize 
Text Align
These classes are for horizontally aligning content. We have .left-align, .right-align and .center-align
```html
 <div>
    <h5 class="left-align">This should be left aligned</h5>
  </div>
  <div>
    <h5 class="right-align">This should be right aligned</h5>
  </div>
  <div>
    <h5 class="center-align">This should be center aligned</h5>
  </div>
```

We provide easy to use classes to hide content on specific screen sizes.

 - .hide 	Hidden for all Devices
 - .hide-on-small-only 	Hidden for Mobile Only
 - .hide-on-med-only 	Hidden for Tablet Only
 - .hide-on-med-and-down 	Hidden for Tablet and Below
 - .hide-on-med-and-up 	Hidden for Tablet and Above
 - .hide-on-large-only 	Hidden for Desktop Only
 
 
 